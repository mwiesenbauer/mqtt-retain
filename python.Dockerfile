# syntax=docker/dockerfile:1.2
FROM python:3-slim

WORKDIR /usr/src/app
ENV PYTHONUNBUFFERED 1

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY config.py .
COPY sub.py .

CMD [ "python3", "sub.py"]
