# syntax=docker/dockerfile:1.2
FROM golang as builder

WORKDIR /usr/src/app

COPY go.mod ./
COPY go.sum ./

RUN go mod download
COPY *.go ./

RUN go build -o pub-with-retain

FROM gcr.io/distroless/base-debian12

WORKDIR /

COPY --from=builder /usr/src/app/pub-with-retain /usr/bin/pub-with-retain

USER nonroot:nonroot

ENTRYPOINT ["/usr/bin/pub-with-retain"]
