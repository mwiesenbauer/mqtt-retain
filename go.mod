module ambos.io/mqtt-retain

go 1.19

require (
	github.com/eclipse/paho.mqtt.golang v1.4.2
	github.com/rs/zerolog v1.24.0
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
)
