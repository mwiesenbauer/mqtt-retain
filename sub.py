import json
import time
import uuid

import config
import paho.mqtt.client as paho_mqtt


def create(client_id: str) -> paho_mqtt.Client:
    cfg = config.get_config()
    client = paho_mqtt.Client(
        paho_mqtt.CallbackAPIVersion.VERSION1,
        transport=cfg.transport,
        clean_session=True
    )
    client.username_pw_set(cfg.user, cfg.password)
    client.connect(host=cfg.host, port=int(cfg.port), keepalive=5)
    return client


def on_connect(client, userdata, flags, rc):
    topic = f"io/ambos/#"
    print(topic)
    client.subscribe(
        topic=topic,
        qos=2,
    )


def on_message(client, userdata, msg):
    payload = msg.payload
    if payload is not None and len(payload) > 0:
        payload = json.dumps(json.loads(payload), indent=2)

    print(msg.topic, "qos:", msg.qos, "retain:", msg.retain, payload)


def main():
    client: paho_mqtt.Client = create(f"python{uuid.uuid4()}")

    client.on_connect = on_connect
    client.on_message = on_message

    client.loop_start()

    time.sleep(7)
    client.loop_stop(force=True)


if __name__ == '__main__':
    main()
