package main

import (
	"context"
	"encoding/json"
	"fmt"
	pahoMQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"time"
)

type config struct {
	connectionURL string
	clientID      string
}

const qualityOfService = 2

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 11*time.Second)
	defer cancel()

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	log.Info().Msg("Starting publisher")
	host := os.Getenv("MQTT_HOST")
	if host == "" {
		host = "127.0.0.1"
	}

	port := os.Getenv("MQTT_PORT")
	if port == "" {
		port = "1883"
	}

	cfg := config{
		connectionURL: fmt.Sprintf("tcp://%s:%s", host, port),
		clientID:      "tower",
	}

	opts := createClientOptions(
		cfg,
		log.Logger,
	)

	opts.SetUsername(os.Getenv("MQTT_USER"))
	opts.SetPassword(os.Getenv("MQTT_PASSWORD"))

	towerConnected := fmt.Sprintf(isConnectedTopic, cfg.ClientID())
	opts.SetOnConnectHandler(onConnectHandler)
	opts.SetConnectionLostHandler(onLostHandler)
	offlinePayload, err := json.Marshal(isConnected{Connected: false, CANConnected: false})
	die(err)

	opts.SetBinaryWill(towerConnected, offlinePayload, qualityOfService, true)

	client, err := connect(opts)
	die(err)

	onlinePayload, err := json.Marshal(isConnected{Connected: true, CANConnected: true})
	die(err)

	client.Publish(towerConnected, qualityOfService, true, onlinePayload)

	<-ctx.Done()
	log.Info().Msg("Terminating publisher")
}

func init() {
	pahoMQTT.ERROR = newLoggerWrapper(zerolog.ErrorLevel)
	pahoMQTT.CRITICAL = newLoggerWrapper(zerolog.WarnLevel)
	pahoMQTT.WARN = newLoggerWrapper(zerolog.WarnLevel)
	pahoMQTT.DEBUG = newLoggerWrapper(zerolog.DebugLevel)
}

type logger struct {
	logLevel    zerolog.Level
	innerLogger zerolog.Logger
}

func newLoggerWrapper(logLevel zerolog.Level) *logger {
	return &logger{
		logLevel: logLevel,
		innerLogger: log.Logger.With().
			Str("component", "paho-mqtt-client").Logger(),
	}
}

func (l logger) Println(v ...interface{}) {
	l.innerLogger.WithLevel(l.logLevel).Msg(fmt.Sprint(v...))
}

func (l logger) Printf(format string, v ...interface{}) {
	l.innerLogger.WithLevel(l.logLevel).Msgf(format, v...)
}

const (
	logLevelDefault                 = "info"
	mqttWriteTimeoutDefault         = 30 * time.Second
	mqttPingTimeoutDefault          = 10 * time.Second
	mqttMaxConnectTimeoutDefault    = 30 * time.Second
	mqttMaxReconnectIntervalDefault = 15 * time.Second
	mqttMaxKeepAliveDefault         = 5 * time.Second
)

func (c config) ConnectionURL() string {
	return c.connectionURL
}

func (c config) ClientID() string {
	return c.clientID
}

func logDisconnectHandler(logger zerolog.Logger) pahoMQTT.ConnectionLostHandler {
	return func(client pahoMQTT.Client, err error) {
		optionsReader := client.OptionsReader()
		logger.Warn().Msgf("%s lost connection: %v", optionsReader.ClientID(), err)
	}
}

func logUnhandledMessage(
	clientName string,
	logger zerolog.Logger,
) func(pahoMQTT.Client, pahoMQTT.Message) {
	return func(client pahoMQTT.Client, msg pahoMQTT.Message) {
		logger.Warn().Msgf(
			"received unhandled message on %s for topic %s with payload %s",
			clientName,
			msg.Topic(),
			msg.Payload(),
		)
	}
}

func createClientOptions(
	cfg config,
	logger zerolog.Logger,
) *pahoMQTT.ClientOptions {
	clientID := getClientID(cfg.ClientID(), "publisher")

	opts := pahoMQTT.NewClientOptions()
	mqttURL := cfg.ConnectionURL()
	logger.Info().Msgf("Connecting to %s", mqttURL)
	opts.AddBroker(mqttURL)
	opts.SetAutoReconnect(true)
	opts.SetConnectRetry(true)
	opts.SetWriteTimeout(mqttWriteTimeoutDefault)
	opts.SetPingTimeout(mqttPingTimeoutDefault)
	opts.SetConnectTimeout(mqttMaxConnectTimeoutDefault)
	opts.SetMaxReconnectInterval(mqttMaxReconnectIntervalDefault)
	opts.SetKeepAlive(mqttMaxKeepAliveDefault)
	opts.SetDefaultPublishHandler(logUnhandledMessage(clientID, logger))
	opts.SetClientID(clientID)
	opts.SetConnectionLostHandler(logDisconnectHandler(logger))

	return opts
}

func connect(opts *pahoMQTT.ClientOptions) (pahoMQTT.Client, error) {
	client := pahoMQTT.NewClient(opts)

	token := client.Connect()
	for !token.WaitTimeout(opts.ConnectTimeout) {
	}

	if err := token.Error(); err != nil {
		return nil, err
	}

	return client, nil
}

func onLostHandler(client pahoMQTT.Client, err error) {
	log.Debug().Err(err).Msg("Client sagt Ciao bella")
}

func onConnectHandler(client pahoMQTT.Client) {
	log.Debug().Msg("Client sagt Hallo")
}

func die(err error) {
	if err == nil {
		return
	}

	log.Fatal().Err(err).Send()
}

func getClientID(macAddr string, role string) string {
	return macAddr + role
}

const isConnectedTopic = "io/ambos/%s/listen/connected"

type isConnected struct {
	Connected    bool `json:"connected"`
	CANConnected bool `json:"can_connected"`
}
