# MQTT retain

Tests retain flag support and behaviour across some MQTT brokers.

The [publisher](pub.go), publishes a message with retain after connection is established and sets a last will on the same topic.
It terminates after eleven seconds, which triggers the last will.

The [subscriber](sub.py), uses a wildcard subscription and for each message on a topic prints the topic, QoS, retain and the payload.
It terminates after seven seconds.

On a compliant broker, the first run should produce:
* publisher publishes and set last will
* subscriber starts and receives message directly or retain, depending on which one comes first
* subscriber terminates, after a restart it receives a message with retain flag
* publisher terminates

The docker-compose.$BROKER_NAME.yml takes care of restarting publisher and subscriber.
