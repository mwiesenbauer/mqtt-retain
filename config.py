import collections
import os

Config = collections.namedtuple('Config', [
    'transport',
    'host',
    'port',
    'user',
    'password',
])


def get_config() -> Config:
    cfg = Config._make([
        os.getenv("MQTT_TRANSPORT", "tcp"),
        os.getenv("MQTT_HOST", "127.0.0.1"),
        os.getenv("MQTT_PORT", 1883),
        os.getenv("MQTT_USER", ""),
        os.getenv("MQTT_PASSWORD", ""),
    ])
    return cfg
